= Write status messages on the console

== Benefits

1. Simple status message workflow
1. Very small codebase
1. Status "`updating`" is done by using `\r`. This means _no_ ncurses or other external dependencies


== Workflow

1. Write a progress message
1. Write a completed message (a completed message is either an `OkMessage` or a `FailMessage`)
1. Repeat as necessary (progress -> completed -> progress -> completed -> ...)

```php
use Cmth\Console\SimpleStatus\Writer;
use Cmth\Console\SimpleStatus\ProgressMessage;
use Cmth\Console\SimpleStatus\OkMessage;
use Cmth\Console\SimpleStatus\FailMessage;

$progressTpl = '[ %4s ]: %s ...';
$okTpl       = '[ %4s ]: %s ... %s';
$failTpl     = '[ %4s ]: %s: %s';

$writer = new Writer($progressTpl, $okTpl, $failTpl);
$writer->write(new ProgressMessage('WIP', 'loading')); // prints "[ WIP  ]: loading ..."

// do real work

if (/*work completed successfully*/) {
    // rewrites progress line to "[  OK  ]: loading ... done"
    $writer->write(new OkMessage(' OK ', 'done'));
} else {
    // rewrites progress line to "[ FAIL ]: loading: file not found: /etc/my.cnf"
    $writer->write(new FailMessage('FAIL', 'file not found: /etc/my.cnf'));
    throw new \Exception("An error occurred, terminating...");
}

// ... or you could just write a bunch of messages and check if a fail message
// has been written and exit

if ($writer->hasWrittenFailMessage()) {
    throw new \Exception("An error occurred, terminating...");
}
```

See the `example/` directory for more examples.

== Limitations

Lines marked with `// error` mean an exception is thrown it those
situations. Lines marked with `// bad` means no exception is thrown but the
program will probably not work as expected.

* The first message must always be a progress message.

```php
$writer = new Writer($progressTpl, $okTpl, $failTpl);
$writer->write(new OkMessage('OK', 'starting up!')); // error
```

 Ok and fail messages are intended to "update" progress messages. If there
 isn't a progress message to update, an exception is thrown.


* You cannot write a progress message followed by a progress message.


```php
$writer = new Writer($progressTpl, $okTpl, $failTpl);
$writer->write(new ProgressMessage('...', 'loading'));
$writer->write(new ProgressMessage('...', 'more loading')); // error
```

* You cannot write a completed message (ok or fail message) before writing a
  progress message:

```php
$writer = new Writer($progressTpl, $okTpl, $failTpl);
$writer->write(new ProgressMessage('...', 'loading'));
$writer->write(new OkMessage('OK', 'done loading!'));
$writer->write(new OkMessage('OK', 'starting up!')); // error
```

* Progress messages should not contain newlines. (This limitation exists due to
progress line re-writing with return carriages, `\r`)

```php
$writer = new Writer($progressTpl, $okTpl, $failTpl);
$writer->write(new ProgressMessage('...', "running:\n\tfdisk")); // bad
```

When updating the progress message, only the `\tfdisk` part of the line will be
re-written because `\r` can only rewrite the current line.

* The ok and fail templates passed to the `Writer` should not end with a
   newline because a newline is appended to them when the template is written
   to the output stream. (This ensures that when the a subsequent progress
   message line is re-written, it does not overwrite part of a completed
   message.)
