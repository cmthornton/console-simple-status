<?php

require __DIR__.'/../autoload.php';

use Cmth\Console\SimpleStatus\Writer;
use Cmth\Console\SimpleStatus\OkMessage;
use Cmth\Console\SimpleStatus\FailMessage;
use Cmth\Console\SimpleStatus\ProgressMessage;

function sleepms($ms)
{
    usleep($ms * 1000);
}

$progTpl = '[ %4s ]  %s ...';
$okTpl   = '[ %4s ]  %s ... %s';
$failTpl = '[ %4s ]  %s: %s';

$w = new Writer($progTpl, $okTpl, $failTpl);

$w->write(new ProgressMessage('', 'starting MySQL'));
sleepms(1000);
$w->write(new OkMessage(' OK ', 'done'));

$w->write(new ProgressMessage('', 'starting nginx'));
sleepms(2500);
$w->write(new FailMessage('FAIL', 'error: file not found: /etc/nginx/nginx.conf'));
