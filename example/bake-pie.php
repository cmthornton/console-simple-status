<?php

require __DIR__.'/../autoload.php';

use Cmth\Console\SimpleStatus\Writer;
use Cmth\Console\SimpleStatus\OkMessage;
use Cmth\Console\SimpleStatus\FailMessage;
use Cmth\Console\SimpleStatus\ProgressMessage;

function sleepms($ms)
{
    usleep($ms * 1000);
}

$progTpl = '%s: %s ...';
$okTpl   = '%s: %s%s';
$failTpl = '%s: %s ... %s';

$w = new Writer($progTpl, $okTpl, $failTpl);

$w->write(new ProgressMessage('1', 'Preheat oven to 400 degrees'));
sleepms(1000);
$w->write(new OkMessage('1', ''));

$w->write(new ProgressMessage('2', 'Place pie in oven'));
sleepms(500);
$w->write(new OkMessage('2', ''));

$w->write(new ProgressMessage('3', 'Bake for 2 hours or until it passes the toothpick test'));
sleepms(1500);
$w->write(new OkMessage('3', ''));

$w->write(new ProgressMessage('4', 'Let cool for 1 hour'));
sleepms(1000);
$w->write(new FailMessage('4', 'Failed! You couldn\'t wait could you? I hope you didn\'t burn your mouth!'));
