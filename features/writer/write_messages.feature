Feature: write status messages
    As a library user
    I need to be able to write status messages on the console
    In order to show my users what task is in progress and the final status of the task


Scenario: Write a progress message
    Given I have a "progress" template "[ %s ] %s ..."
    And I have an "ok" template "[ %s ] %s ... %s"
    And I have a "fail" template "[ %s ] %s: %s"
    When I write a "progress" message "loading" with status "    "
    Then written fail message should be "false"
    And I should get the following freeform-escaped stream:
    """
    [      ] loading ...
    """


Scenario: Write a progress message then update it to a progress message
    Given I have a "progress" template "[ %s ] %s ..."
    And I have an "ok" template "[ %s ] %s ... %s"
    And I have a "fail" template "[ %s ] %s: %s"
    And I have written a "progress" message "loading" with status "    "
    When I write a "progress" message "loading more" with status "    "
    Then a "Cmth\Console\SimpleStatus\UnexpectedProgressMessageException" exception should be thrown


Scenario: Write a completed message then update it with a completed message
    Given I have a "progress" template "[ %s ] %s ..."
    And I have an "ok" template "[ %s ] %s ... %s"
    And I have a "fail" template "[ %s ] %s: %s"
    And I have written a "progress" message "loading" with status "    "
    And I have written an "ok" message "done" with status " OK "
    When I write an "ok" message "loading more" with status " OK "
    Then a "Cmth\Console\SimpleStatus\UnexpectedCompletedMessageException" exception should be thrown


Scenario: Write a progress message then update it to a fail message
    Given I have a "progress" template "[ %s ] %s ..."
    And I have an "ok" template "[ %s ] %s ... %s"
    And I have a "fail" template "[ %s ] %s: %s"
    And I have written a "progress" message "loading" with status "    "
    When I write a "fail" message "error: E123" with status "FAIL"
    Then written fail message should be "true"
    And I should get the following freeform-escaped stream:
    """
    [      ] loading ...
    \r                    \r
    [ FAIL ] loading: error: E123\n
    """


Scenario: Write a progress message then update it to an ok message
    Given I have a "progress" template "[ %s ] %s ..."
    And I have an "ok" template "[ %s ] %s ... %s"
    And I have a "fail" template "[ %s ] %s: %s"
    And I have written a "progress" message "loading" with status "    "
    When I write an "ok" message "done" with status " OK "
    Then written fail message should be "false"
    And I should get the following freeform-escaped stream:
    """
    [      ] loading ...
    \r                    \r
    [  OK  ] loading ... done\n
    """


Scenario: Write a progress message then update it to a fail message, then write a progress message
    Given I have a "progress" template "[ %s ] %s ..."
    And I have an "ok" template "[ %s ] %s ... %s"
    And I have a "fail" template "[ %s ] %s: %s"
    And I have written a "progress" message "loading" with status "    "
    And I have written a "fail" message "error: command not found: /bin/cat" with status "FAIL"
    When I write a "progress" message "inspecting PATH" with status "    "
    Then I should get the following freeform-escaped stream:
    """
    [      ] loading ...
    \r                    \r
    [ FAIL ] loading: error: command not found: /bin/cat\n
    [      ] inspecting PATH ...
    """


Scenario: Write a progress message then update it to an ok message, then write a progress message
    Given I have a "progress" template "[ %s ] %s ..."
    And I have an "ok" template "[ %s ] %s ... %s"
    And I have a "fail" template "[ %s ] %s: %s"
    And I have written a "progress" message "loading" with status "    "
    And I have written a "ok" message "done" with status " OK "
    When I write a "progress" message "inspecting PATH" with status "    "
    Then I should get the following freeform-escaped stream:
    """
    [      ] loading ...
    \r                    \r
    [  OK  ] loading ... done\n
    [      ] inspecting PATH ...
    """


Scenario: Lots of messages, somewhat unusual format
    Given I have a "progress" template "%s%s ..."
    And I have an "ok" template "%s: %s ... %s!"
    And I have a "fail" template "%s: %s: %s!"
    And I have written a "progress" message "Loading faces" with status ""
    And I have written a "ok" message "finished" with status "1"
    And I have written a "progress" message "Loading environment" with status ""
    And I have written a "ok" message "finished" with status "2"
    And I have written a "progress" message "Loading equipment" with status ""
    And I have written a "ok" message "finished" with status "3"
    And I have written a "progress" message "Loading audio" with status ""
    When I write a "fail" message "error: speakers not found" with status "4"
    Then I should get the following freeform-escaped stream:
    """
    Loading faces ...
    \r                 \r
    1: Loading faces ... finished!\n
    Loading environment ...
    \r                       \r
    2: Loading environment ... finished!\n
    Loading equipment ...
    \r                     \r
    3: Loading equipment ... finished!\n
    Loading audio ...
    \r                 \r
    4: Loading audio: error: speakers not found!\n
    """
