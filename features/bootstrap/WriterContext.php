<?php

//require_once __DIR__.'/../../autoload.php';

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Cmth\Console\SimpleStatus\Writer;
use Cmth\Console\SimpleStatus\OkMessage;
use Cmth\Console\SimpleStatus\FailMessage;
use Cmth\Console\SimpleStatus\ProgressMessage;
use Cmth\Console\SimpleStatus\UnexpectedMessageException;

/**
 * Feature context for message writer
 */
class WriterContext implements Context, SnippetAcceptingContext
{
    /**
     * Template for progress messages
     *
     * @var string
     */
    protected $progressTemplate;

    /**
     * Template for ok messages
     *
     * @var string
     */
    protected $okTemplate;

    /**
     * Template for fail messages
     *
     * @var string
     */
    protected $failTemplate;

    /**
     * status message writer
     *
     * @var Writer
     */
    protected $writer;

    /**
     * stream output is written to
     *
     * @var resource
     */
    protected $outputStream;

    /**
     * exception class name
     *
     * @var resource
     */
    protected $exceptionClass;

    /**
     * @Given /^I have an? "(?P<type>[^"]*?)" template "(?P<message>[^"]*?)"$/
     */
    public function iHaveATemplate($type, $template)
    {
        switch (strtolower($type)) {
        case 'ok':
            $this->okTemplate = $template;
            break;
        case 'fail':
            $this->failTemplate = $template;
            break;
        case 'progress':
            $this->progressTemplate = $template;
            break;
        default:
            throw new PendingException();
            break;
        }
    }

    /**
     * @When /^I write an? "(?P<type>[^"]*?)" message "(?P<message>[^"]*?)" with status "([^"]*?)"$/
     */
    public function iWriteAMessage($type, $message, $status)
    {
        try {
            $this->performWrite($type, $message, $status);
        } catch (UnexpectedMessageException $e) {
            $this->exceptionClass = get_class($e);
        }
    }

    /**
     * @BeforeScenario
     */
    public function before(BeforeScenarioScope $scope)
    {
        $this->outputStream = fopen('php://memory', 'a+');
    }

    /**
     * @AfterScenario
     */
    public function after(AfterScenarioScope $scope)
    {
        unset($this->writer);
        unset($this->outputStream);
    }

    /**
     * Escapes special characters (\n, \r, etc.) in a string
     *
     * @param string $message
     *
     * @return string The escaped string
     */
    protected function escape($message)
    {
        $trans = array(
                "\\" => '\\',
                "\n" => '\\n',
                "\t" => '\\t',
                "\r" => '\\r',
            );

        return strtr($message, $trans);
    }

    /**
     * Converts a PyStringNode to a freeform-escaped stream
     *
     * @notes "freeform" meaning actual newlines are ignored in the stream. This
     * is similar to "freeform" regex, except spaces are not ignored in the
     * freeform-escaped stream.
     *
     * @param PyStringNode $psn Multi-line input string
     *
     * @return string Escaped string
     */
    protected function pyStringNode2FreeformEscapedStream(PyStringNode $psn)
    {
        return $this->escape(implode('', explode("\n", $psn)));
    }

    /**
     * Gets data written to the output stream
     *
     * @return string Escaped string
     */
    protected function getEscapedOutputStream()
    {
        // need to rewind the stream first because we've been writing to it and
        // the pointer is pointing to the end of the stream
        rewind($this->outputStream);

        return $this->escape(stream_get_contents($this->outputStream));
    }

    /**
     * @Then I should get the following freeform-escaped stream:
     */
    public function iShouldGetTheFollowingFreeformEscapedStream(PyStringNode $expected)
    {
        $expected = $this->pyStringNode2FreeformEscapedStream($expected);
        $actual = $this->getEscapedOutputStream();

        if ($expected !== $actual) {
            throw new Exception("Actual stream is:\n".$actual);
        }
    }

    /**
     * @Then /^written fail message should be "([^"]+?)"$/
     */
    public function writtenFailMessageShouldBe($truthy)
    {
        $expected = "true" === $truthy;
        $actual = $this->writer->hasWrittenFailMessage();

        if ($expected !== $actual) {
            throw new Exception("Actual value is:\n".$actual);
        }
    }

    /**
     * Builds status writer, if necessary
     *
     * @return Writer
     */
    protected function buildWriter()
    {
        if (!$this->writer) {
            $this->writer = new Writer(
                $this->progressTemplate,
                $this->okTemplate,
                $this->failTemplate,
                $this->outputStream
            );
        }

        return $this->writer;
    }

    /**
     * Writes status messages
     *
     * @param string $type    Type of status message
     * @param string $message Status message text
     * @param string $status  Status message status text
     */
    protected function performWrite($type, $message, $status)
    {
        $this->buildWriter();

        switch (strtolower($type)) {
        case 'ok':
            $this->writer->write(new OkMessage($status, $message));
            break;
        case 'fail':
            $this->writer->write(new FailMessage($status, $message));
            break;
        case 'progress':
            $this->writer->write(new ProgressMessage($status, $message));
            break;
        default:
            throw new PendingException();
            break;
        }
    }

    /**
     * @Given /^I have written an? "([^"]*?)" message "([^"]*?)" with status "([^"]*?)"$/
     */
    public function iHaveWrittenAMessage($type, $message, $status)
    {
        $this->performWrite($type, $message, $status);
    }

    /**
     * @Then /^an? "([^"]*?)" exception should be thrown$/
     */
    public function anExceptionShouldBeThrown($fqcn)
    {
        if ($fqcn !== $this->exceptionClass) {
            throw new Exception("Actual exception class is: ".$this->exceptionClass);
        }
    }
}
