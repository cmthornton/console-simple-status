<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Progress message
 */
class ProgressMessage extends BaseMessage
{
    /**
     * {@inheritDoc}
     */
    public function isProgressMessage()
    {
        return true;
    }
}
