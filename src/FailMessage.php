<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Fail message
 */
class FailMessage extends BaseMessage implements CompletedMessageInterface
{
    /**
     * {@inheritDoc}
     */
    public function isFailMessage()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function isOkMessage()
    {
        return false;
    }
}
