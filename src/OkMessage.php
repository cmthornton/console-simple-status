<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Ok message
 */
class OkMessage extends BaseMessage implements CompletedMessageInterface
{
    /**
     * {@inheritDoc}
     */
    public function isFailMessage()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function isOkMessage()
    {
        return true;
    }
}
