<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Exception thrown when not excepting a progress message
 */
class UnexpectedProgressMessageException extends UnexpectedMessageException
{
}
