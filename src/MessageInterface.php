<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Interface for messages
 */
interface MessageInterface
{
    /**
     * Get status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage();

    /**
     * True if the message is a progress message, otherwise false
     *
     * @return boolean
     */
    public function isProgressMessage();
}
