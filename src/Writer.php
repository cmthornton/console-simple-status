<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Writes status messages
 */
class Writer
{
    /**
     * Template for OkMessages
     *
     * @var string
     */
    protected $okTemplate;

    /**
     * Template for FailMessages
     *
     * @var string
     */
    protected $failTemplate;

    /**
     * Template for ProgressMessages
     *
     * @var string
     */
    protected $progressTemplate;

    /**
     * Last progress message
     *
     * @var MessageInterface
     */
    protected $lastProgressMessage;

    /**
     * True if next message is expected to be a progress message
     *
     * @var string
     */
    protected $expectProgressMessage;

    /**
     * Stream to write messages to
     *
     * @var resource
     */
    protected $stream;

    /**
     * True if writer has written a fail message
     *
     * @var boolean
     */
    protected $hasWrittenFailMessage = false;

    /**
     * Constructor
     *
     * @param string   $progress Progress message template
     * @param string   $ok       OK message template
     * @param string   $fail     Fail message template
     * @param resource $stream   Output stream, defaults to stdout
     */
    public function __construct($progress, $ok, $fail, $stream = null)
    {
        $this->progressTemplate = strval($progress);
        $this->okTemplate = strval($ok);
        $this->failTemplate = strval($fail);
        $this->expectProgressMessage = true;
        $this->stream = $stream;

        // use STDOUT if no stream given
        if ($stream === null) {
            $this->stream = \STDOUT;
        }

        // make sure stream is writable
        $mode = stream_get_meta_data($this->stream)['mode'];
        if (false === strpos($mode, 'w')) {
            throw new \UnexpectedValueException(
                sprintf('Output stream (%s) is not writable', $this->stream)
            );
        }
    }

    /**
     * Returns a string representation of the message's type
     *
     * @param MessageInterface $m
     *
     * @return string
     */
    protected function getType(MessageInterface $m)
    {
        if ($m->isProgressMessage()) {
            return 'progress';

        // TODO since we don't have a progress message, it's assumed to be a
        // message that implements the CompletedMessageInterface. Perhaps this
        // should change
        } elseif ($m->isOkMessage()) {
            return 'ok';
        } elseif ($m->isFailMessage()) {
            return 'fail';
        }

        return 'unknown';
    }

    /**
     * Overwrites the last progress message by re-writing the message with
     * spaces and leaves the cursor at the beginning of the line to overwrite
     * the next message
     *
     * @return void
     */
    protected function clearLastProgressMessage()
    {
        $status = $this->lastProgressMessage->getStatus();
        $message = $this->lastProgressMessage->getMessage();
        $output = sprintf($this->progressTemplate, $status, $message);

        $whiteout = preg_replace('/./', ' ', $output);

        // move to beginning of line
        fprintf($this->stream, "\r");

        // over write the old message with spaces
        fprintf($this->stream, $whiteout);

        // move to beginning of line so the completed message can update the old
        // message as ok/fail
        fprintf($this->stream, "\r");
    }

    /**
     * Write a message
     *
     * @param  MessageInterface $m
     * @return int              The length of the string written
     */
    public function write(MessageInterface $m)
    {
        $status = $m->getStatus();
        $message = $m->getMessage();

        switch ($this->accept($m)) {
        case 'ok':
            $this->clearLastProgressMessage();
            $template = $this->okTemplate."\n";
            $last = $this->lastProgressMessage->getMessage();
            $len = fprintf($this->stream, $template, $status, $last, $message);
            break;

        case 'fail':
            $this->clearLastProgressMessage();
            $template = $this->failTemplate."\n";
            $last = $this->lastProgressMessage->getMessage();
            $len = fprintf($this->stream, $template, $status, $last, $message);
            $this->hasWrittenFailMessage = true;
            break;

        case 'progress':
            $template = $this->progressTemplate;
            $len = fprintf($this->stream, $template, $status, $message);
            $this->lastProgressMessage = $m;
            break;

        default:
            throw new \UnexpectedValueException('Unknown message type');
            break;
        }

        $this->expectProgressMessage = !$this->expectProgressMessage;

        return $len;
    }

    /**
     * Returns the a string representation of the "type" of message, if
     * accepted. Otherwise, an exception is thrown because the message type was
     * not expected.
     *
     * @param  MessageInterface $m
     * @return string
     */
    protected function accept(MessageInterface $m)
    {
        if ($this->expectProgressMessage === $m->isProgressMessage()) {
            return $this->getType($m);
        }

        // got an unexpected message type, throw the appropriate exception
        if ($m->isProgressMessage()) {
            throw new UnexpectedProgressMessageException(
                'Expected a completed message, but got a progress message'
            );
        }

        throw new UnexpectedCompletedMessageException(
            'Expected a progress message, but got a completed message'
        );
    }

    /**
     * Returns true if a fail message has been written, otherwise false.
     *
     * @return boolean
     */
    public function hasWrittenFailMessage()
    {
        return $this->hasWrittenFailMessage;
    }
}
