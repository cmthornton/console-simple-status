<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Base message
 */
abstract class BaseMessage implements MessageInterface
{
    /**
     * Message
     *
     * @var string
     */
    protected $_message;

    /**
     * Status
     *
     * @var string
     */
    protected $_status;

    /**
     * Constructor
     *
     * @param string $status
     * @param string $message
     */
    public function __construct($status, $message)
    {
        $this->_status = strval($status);
        $this->_message = strval($message);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     * {@inheritDoc}
     */
    public function getMessage()
    {
        return $this->_message;
    }

    /**
     * {@inheritDoc}
     */
    public function isProgressMessage()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {
        return $this->getMessage();
    }
}
