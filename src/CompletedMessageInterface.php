<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Interface for completed messages
 */
interface CompletedMessageInterface
{
    /**
     * True if the message is an ok message, otherwise false
     *
     * @return boolean
     */
    public function isOkMessage();

    /**
     * True if the message is a fail message, otherwise false
     *
     * @return boolean
     */
    public function isFailMessage();
}
