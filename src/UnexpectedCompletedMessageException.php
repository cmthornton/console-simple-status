<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Exception thrown when not excepting a completed message
 */
class UnexpectedCompletedMessageException extends UnexpectedMessageException
{
}
