<?php

namespace Cmth\Console\SimpleStatus;

/**
 * Base exception class
 */
class UnexpectedMessageException extends \Exception
{
}
